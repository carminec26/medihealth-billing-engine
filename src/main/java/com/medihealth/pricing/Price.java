package com.medihealth.pricing;

import java.util.Optional;

public interface Price {

    double calculatePrice(Optional<Integer> discount);

}
