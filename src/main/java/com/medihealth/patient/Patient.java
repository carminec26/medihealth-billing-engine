package com.medihealth.patient;

import java.util.UUID;

public class Patient {

    private String id;

    private String firstName;

    private String lastName;

    private int age;

    private boolean isMediHealthInsured;

    public Patient(String firstName, String lastName, int age, boolean isMediHealthInsured) {
        this.id = UUID.randomUUID().toString();
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.isMediHealthInsured = isMediHealthInsured;
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public boolean isMediHealthInsured() {
        return isMediHealthInsured;
    }

}
