package com.medihealth.service;

public enum ServiceType {

    DIAGNOSIS("Diagnosis", 60, 0),
    XRAY("X-Ray", 150, 0),
    BLOODTEST("Blood Test", 78, 0),
    ECG("ECG", 200.40, 0),
    VACCINE("Vaccine", 27.50, 15);

    private final String name;
    private final double baseCost;
    private final double doseCost;

    ServiceType(String name, double baseCost, double doseCost) {
        this.name = name;
        this.baseCost = baseCost;
        this.doseCost = doseCost;
    }

    public String getName() {
        return name;
    }

    public double getBaseCost() {
        return baseCost;
    }

    public double getDoseCost() {
        return doseCost;
    }
}
