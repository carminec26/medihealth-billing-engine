package com.medihealth;

import com.medihealth.patient.Patient;
import com.medihealth.patient.PractitionerType;
import com.medihealth.pricing.Discount;
import com.medihealth.service.Service;
import com.medihealth.service.ServiceType;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

public class BillingService {

    private static final Locale EN_LOCALE = new Locale("en", "GB");
    private static final NumberFormat numberFormat = NumberFormat.getCurrencyInstance(EN_LOCALE);

    private List<Discount> discountRules = generateDiscountRules();

    public double calculateBill(Patient patient, List<Service> services) {
        return services.stream()
                        .map(s -> s.calculatePrice(getDiscount(patient, s, discountRules)))
                        .reduce(0d, (a, b) -> a + b);
    }

    public String printBill(Patient patient, List<Service> services, double cost) {
        StringBuilder builder = new StringBuilder();
        builder.append("Patient: " + patient.getFirstName() + " " + patient.getLastName());
        builder.append(System.lineSeparator());

        for(Service s: services) {
            builder.append("Service: " + s.getServiceType().getName());
            builder.append(System.lineSeparator());
            builder.append("Practitioner: " + s.getPractitioner().getName());
            builder.append(System.lineSeparator());
        }

        builder.append("Total Cost: " + numberFormat.format(cost));
        builder.append(System.lineSeparator());

        return builder.toString();
    }

    private Optional<Integer> getDiscount(Patient patient, Service service, List<Discount> discountRules) {

        Integer discountValue = discountRules.stream()
                                .filter(d -> d.applicable(patient, service))
                                .map(d -> d.getPercentage())
                                .reduce(0, (a, b) -> a + b);

        return Optional.of(discountValue);

    }

    public static List<Discount> generateDiscountRules() {
        List<Discount> discounts = new ArrayList<>();

        Discount seniorBetween65And70 = new Discount(60,  (Patient p, Service s) ->
                                                                        p.getAge() >= 65 && p.getAge() <= 70);

        Discount seniorOver70 = new Discount(90, (Patient p, Service s) ->
                                                                p.getAge() > 70);

        Discount childrenUnder5 = new Discount(40, (Patient p, Service s) ->
                                                                p.getAge() < 5);

        Discount compoundDiscount = new Discount(15, (Patient p, Service s) ->
                                                                p.isMediHealthInsured() &&
                                                                s.getPractitioner() == PractitionerType.MEDIHEALTH &&
                                                                s.getServiceType() == ServiceType.BLOODTEST);

        discounts.add(seniorBetween65And70);
        discounts.add(seniorOver70);
        discounts.add(childrenUnder5);
        discounts.add(compoundDiscount);

        return discounts;
    }

    public List<Discount> getDiscountRules() {
        return discountRules;
    }

    public void setDiscountRules(List<Discount> discountRules) {
        this.discountRules = discountRules;
    }

}
