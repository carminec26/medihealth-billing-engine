package com.medihealth.patient;

public enum PractitionerType {

    MEDIHEALTH("MediHealth"),
    NHS("NHS");

    private final String name;

    PractitionerType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
