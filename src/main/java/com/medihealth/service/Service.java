package com.medihealth.service;

import com.medihealth.patient.PractitionerType;
import com.medihealth.pricing.Price;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

public class Service implements Price {

    private ServiceType serviceType;
    private PractitionerType practitioner = PractitionerType.MEDIHEALTH;
    private int numberOfDoses = 0;

    public Service(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    @Override
    public double calculatePrice(Optional<Integer> discount) {

        double price = serviceType.getBaseCost() + (serviceType.getDoseCost() * numberOfDoses);

        if(discount.isPresent()) {
            double res = price * discount.get().intValue();
            double discountValue = res/100;
            return formatPrice(price - discountValue);
        }

        return formatPrice(price);
    }

    private double formatPrice(double price) {
        return BigDecimal.valueOf(price).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public int getNumberOfDoses() {
        return numberOfDoses;
    }

    public void setNumberOfDoses(int numberOfDoses) {
        this.numberOfDoses = numberOfDoses;
    }

    public PractitionerType getPractitioner() {
        return practitioner;
    }

    public void setPractitioner(PractitionerType practitioner) {
        this.practitioner = practitioner;
    }

}
