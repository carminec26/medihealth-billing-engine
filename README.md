# MediHealth Center Application

The solution has been implemented with:

* Java 8
* JUnit 5

## Getting started

1. Clone this repository:

		git clone https://carminec26@bitbucket.org/carminec26/medihealth-billing-engine.git

2. Change directory into your clone:

		cd medihealth-billing-engine

3. Run JUnit Test or Main class

## Design

The application has been designed using several classes representing the domain:

* Patient: a patient that can use several services
* Service: a medical service
* Practitioner: a practitioner and it is associated with the service

Billing service is the service that 

* calculate the total cost of services taken by a patient, including discounts
* print the bill

Discounts are modeled with a class that has a percentage value and a Predicate that defines the conditions for the
discount to be applied.

The predicate is of type DiscountPredicate which is a functional interface. In this way it is possible to create 
discount in a flexible way using lambdas. The creation of default discount rules is in the billing service itself
for simplicity but the billing engine can work with other rules passed as List<Discount> for example defined in a file.


