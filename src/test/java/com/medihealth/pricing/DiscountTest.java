package com.medihealth.pricing;

import com.medihealth.patient.Patient;
import com.medihealth.service.Service;
import com.medihealth.service.ServiceType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DiscountTest {

    private static Service service;
    private static Discount ageAboveXDiscount;

    @BeforeAll
    static void setUp() {
        service = new Service(ServiceType.DIAGNOSIS);
        ageAboveXDiscount = new Discount(90, (Patient p, Service s) -> p.getAge() > 70);
    }

    @Test
    void shouldDiscountNotBeApplicableWhenConditionsAreNotVerified() {
        Patient patient = new Patient("John", "White", 68, false);

        boolean result = ageAboveXDiscount.applicable(patient, service);

        assertFalse(result);
    }

    @Test
    void shouldDiscountBeApplicableWhenConditionsAreVerified() {
        Patient patient = new Patient("John", "White", 78, false);

        boolean result = ageAboveXDiscount.applicable(patient, service);

        assertTrue(result);
    }

}