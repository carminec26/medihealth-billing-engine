package com.medihealth;

import com.medihealth.patient.Patient;
import com.medihealth.pricing.Discount;
import com.medihealth.service.Service;
import com.medihealth.service.ServiceType;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        BillingService billingService = new BillingService();
        List<Discount> discountRules = billingService.getDiscountRules();

        // Patient with one service
        Patient johnWithe = new Patient("John", "White", 68, false);
        Service diagnosis = new Service(ServiceType.DIAGNOSIS);

        double costSingleService = billingService.calculateBill(johnWithe, Arrays.asList(diagnosis));

        System.out.println(billingService.printBill(johnWithe, Arrays.asList(diagnosis), costSingleService));

        // Patient with more than one service
        Patient carlDonovan = new Patient("Carl", "Donovan", 80, false);
        Service ecg = new Service(ServiceType.ECG);
        Service vaccines = new Service(ServiceType.VACCINE);
        vaccines.setNumberOfDoses(3);
        List<Service> services = Arrays.asList(ecg, vaccines);

        double costMultipleServices = billingService.calculateBill(carlDonovan, services);

        System.out.println(billingService.printBill(carlDonovan, services, costMultipleServices));
    }

}
