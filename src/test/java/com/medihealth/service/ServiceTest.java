package com.medihealth.service;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ServiceTest {

    @Test
    void shouldCalculatePriceForDiagnosis() {
        Service service = new Service(ServiceType.DIAGNOSIS);

        double price = service.calculatePrice(Optional.empty());

        assertEquals(60, price);
    }

    @Test
    void shouldCalculatePriceForXRay() {
        Service service = new Service(ServiceType.XRAY);

        double price = service.calculatePrice(Optional.empty());

        assertEquals(150, price);
    }

    @Test
    void shouldCalculatePriceForBloodTest() {
        Service service = new Service(ServiceType.BLOODTEST);

        double price = service.calculatePrice(Optional.empty());

        assertEquals(78, price);
    }

    @Test
    void shouldCalculatePriceForBloodEcg() {
        Service service = new Service(ServiceType.ECG);

        double price = service.calculatePrice(Optional.empty());

        assertEquals(200.40, price);
    }

    @Test
    void shouldCalculatePriceForVaccines() {
        Service service = new Service(ServiceType.VACCINE);
        service.setNumberOfDoses(5);

        double price = service.calculatePrice(Optional.empty());

        assertEquals(27.50 + (15 * 5), price);
    }

}