package com.medihealth.pricing;

import com.medihealth.patient.Patient;
import com.medihealth.service.Service;

@FunctionalInterface
public interface DiscountPredicate {
    boolean testDiscount(Patient patient, Service service);
}
