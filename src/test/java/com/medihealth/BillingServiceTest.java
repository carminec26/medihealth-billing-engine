package com.medihealth;

import com.medihealth.patient.Patient;
import com.medihealth.service.Service;
import com.medihealth.service.ServiceType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BillingServiceTest {

    private BillingService billingService;

    @BeforeEach
    void setUp() {
        billingService = new BillingService();
    }

    //Senior citizens (between 65 and 70) receive a 60% discount
    @Test
    public void shouldGet60PercentDiscountWhenPatientAgeBetween65And70() {
        Patient patient = new Patient("John", "White", 69, false);
        Service service = new Service(ServiceType.ECG);

        double cost = billingService.calculateBill(patient, Arrays.asList(service));

        assertEquals(80.16, cost);
    }

    //Senior citizens (over 70) receive a 90% discount
    @Test
    public void shouldGet90PercentDiscountWhenPatientAgeOver70() {
        Patient patient = new Patient("John", "White", 80, false);
        Service service = new Service(ServiceType.ECG);

        double cost = billingService.calculateBill(patient, Arrays.asList(service));

        assertEquals(20.04, cost);
    }

    //Children under 5 receive 40% discount
    @Test
    public void shouldGet40PercentDiscountWhenPatientAgeUnder5() {
        Patient patient = new Patient("John", "White", 2, false);
        Service service = new Service(ServiceType.ECG);

        double cost = billingService.calculateBill(patient, Arrays.asList(service));

        assertEquals(120.24, cost);
    }

    //Patients with "MediHealth Health insurance" get additional 15% discount on Blood test when they are diagnosed
    //by a MediHealth practitioner
    @Test
    public void shouldGetAdditional15PercentDiscount() {
        Patient patient = new Patient("John", "White", 2, true);
        Service service = new Service(ServiceType.BLOODTEST);

        double cost = billingService.calculateBill(patient, Arrays.asList(service));

        //I assume that the discount is accumulative that means 40% + 15% = 55% to apply
        //35.10 = 78 - 55%
        assertEquals(35.10, cost);
    }

    // No Discount Applied when condition are not satisfied
    @Test
    public void shouldNotGetAnyDiscountWhenConditionsAreNotSatisfied() {
        Patient patient = new Patient("John", "White", 20, false);
        Service service = new Service(ServiceType.ECG);

        double cost = billingService.calculateBill(patient, Arrays.asList(service));

        assertEquals(200.40, cost);
    }

    // Cost calculation when a patient has more than one service
    @Test
    public void shouldCalculateTotalCostWithoutDiscountWhenPatientHasMoreThanOneService() {
        Patient patient = new Patient("John", "White", 20, false);
        List<Service> services = Arrays.asList(new Service(ServiceType.ECG), new Service(ServiceType.XRAY));

        double cost = billingService.calculateBill(patient, services);

        assertEquals(200.40 + 150, cost);
    }

    // Cost calculation with discount when a patient has more than one service
    @Test
    public void shouldCalculateTotalCostWithDiscountWhenPatientHasMoreThanOneService() {
        Patient patient = new Patient("John", "White", 80, false);
        List<Service> services = Arrays.asList(new Service(ServiceType.ECG), new Service(ServiceType.XRAY));

        double cost = billingService.calculateBill(patient, services);

        assertEquals(20.04 + 15, cost);
    }

}