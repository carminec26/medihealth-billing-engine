package com.medihealth.pricing;

import com.medihealth.patient.Patient;
import com.medihealth.service.Service;

public class Discount {

    private int percentage;
    private DiscountPredicate discountPredicate;

    public Discount(int percentage, DiscountPredicate discountPredicate) {
        this.percentage = percentage;
        this.discountPredicate = discountPredicate;
    }

    public boolean applicable(Patient patient, Service service) {
        return discountPredicate.testDiscount(patient, service);
    }

    public int getPercentage() {
        return percentage;
    }

}
